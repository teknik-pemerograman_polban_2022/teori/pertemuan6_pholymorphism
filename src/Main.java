import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Pokemon pokemon1 = new Bulbasaur("Bulbasaur", 100, 100,
                new ArrayList<String>(Arrays.asList("GRASS", "POISON")));
        Pokemon pokemon2 = new Charmander("Charmander", 100, 100, new ArrayList<String>(Arrays.asList("FIRE")));

        System.out.println("Welcome to battle!\n");

        System.out.println("===============================");
        System.out.println("\nYour pokemon:");
        System.out.println(pokemon1.ToString());
        System.out.println("\n\nOpponent:");
        System.out.println(pokemon2.ToString());
        System.out.println("===============================");

        System.out.println();
        pokemon1.attack(pokemon2);
        System.out.println();
        System.out.println("===============================");
        System.out.println("\nYour pokemon:");

        System.out.println(pokemon1.ToString());
        System.out.println("\n\nOpponent:");
        System.out.println(pokemon2.ToString());
        System.out.println("===============================");

        System.out.println("\n\nOh? Your pokemon evolved");
        pokemon1 = new Ivysaur("Ivysaur", pokemon1.getMP(), pokemon1.getMP(), pokemon1.getTipe());
        System.out.println("===============================");
        System.out.println("\nYour pokemon:");

        System.out.println(pokemon1.ToString());
        System.out.println("\n\nOpponent:");
        System.out.println(pokemon2.ToString());
        System.out.println("===============================");
    }
}
