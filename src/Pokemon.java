import java.util.ArrayList;

abstract class Pokemon implements TypeMatch {
    private String nama;
    private int HP;
    private int MP;
    private ArrayList<String> tipe;

    public String getNama() {
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getHP() {
        return this.HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getMP() {
        return this.MP;
    }

    public void setMP(int MP) {
        this.MP = MP;
    }

    public Pokemon(String nama, int HP, int MP, ArrayList<String> Tipe) {
        this.nama = nama;
        this.HP = HP;
        this.MP = MP;
        this.tipe = new ArrayList<String>();
        Tipe.forEach((n) -> this.tipe.add(n));
    }

    public void addTipe(String tipe) {
        this.tipe.add(tipe);
    }

    public ArrayList<String> getTipe() {
        return tipe;
    }

    abstract void attack(Pokemon target);

    public boolean isDie() {
        if (this.HP < 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isHaveMP(int targetToUse) {
        if (this.MP < targetToUse) {
            return true;
        } else {
            return false;
        }
    }

    public String ToString() {
        // TODO Auto-generated method stub
        String result = "";
        result += "Name : " + this.getNama();
        result += "\nType : " + this.tipe.toString();
        result += "\nHP   : " + this.getHP();
        result += "\nMP   : " + this.getMP();
        if (this.isDie()) {
            result += "\nStatus : died";
        } else {
            result += "\nStatus : alive";
        }

        return result;
    }
}
