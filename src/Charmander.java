import java.util.ArrayList;

public class Charmander extends Pokemon {

    public Charmander(String nama, int HP, int MP, ArrayList<String> Tipe) {
        super(nama, HP, MP, Tipe);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean isWeakTo(Pokemon Target) {
        // TODO Auto-generated method stub
        if (Target.getTipe().contains("WATER") || Target.getTipe().contains("GROUND")
                || Target.getTipe().contains("ROCK")) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    void attack(Pokemon target) {
        // TODO Auto-generated method stub
        if (!this.isDie() || this.isHaveMP(10)) {
            System.out.println(this.getNama() + " use Fire Ball!");
            this.setMP(getMP() - 5);
            if (target.isWeakTo(this)) {
                System.out.println("Its very effective!");
                target.setHP(getHP() - 50);
            } else if (this.isWeakTo(target)) {
                System.out.println("Its not very effective!");
                target.setHP(getHP() - 1);
            }
        } else {
            System.out.println(this.getNama() + " can't use that move");
        }
    }

}
